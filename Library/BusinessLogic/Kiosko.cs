﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using BE = Kiosko.Library.BusinessEntity;
using DA = Kiosko.Library.DataAccess;
using NA = Kiosko.Library.NetworkAccess;

namespace Kiosko.Library.BusinessLogic
{
    public class Kiosko
    {

        /// <summary>
        /// Listar el ultimo monitoreo del kioskos
        /// </summary>
        /// <returns></returns>
        public List<BE.Kiosko> ListarMonitoreo()
        {
            try
            {
                var lstBeKioskos = new DA.Kiosko().ListarMonitoreo();

                return lstBeKioskos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtener el ultimo monitoreo del kiosko
        /// </summary>
        /// <param name="hostname">Nombre del Kiosko</param>
        /// <returns></returns>
        public BE.Kiosko ObtenerMonitoreo(string hostname)
        {
            try
            {
                return new DA.Kiosko().ObtenerMonitoreo(hostname);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Reinicio del kiosko
        /// </summary>
        /// <param name="hostname">Nombre del Kiosko</param>
        /// <returns></returns>
        public bool Reiniciar(string hostname)
        {
            try
            {
                string msgTrue = "RESTORING";
                string msg = new NA.Kiosko().Restart(hostname);
                return msg.ToUpper().Equals(msgTrue);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtener el monitoreo del kiosko del momento
        /// </summary>
        /// <param name="hostname">Nombre del Kiosko</param>
        /// <returns></returns>
        public BE.Kiosko Estado(string hostname)
        {
            try
            {
                BE.Kiosko beEstado = new DA.Kiosko().ObtenerMonitoreo(hostname);

                if (beEstado != null)
                {
                    string json = new NA.Kiosko().Status(hostname);

                    if (json.Trim().Length > 0)
                    {
                        var definition = new { Version = "", Type = "", Number = "", };
                        var status = JsonConvert.DeserializeAnonymousType(json, definition);

                        beEstado.Version = status.Version;
                        beEstado.Sesion = status.Number;
                        beEstado.FechaHora = DateTime.Now;
                    }
                }

                return beEstado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
