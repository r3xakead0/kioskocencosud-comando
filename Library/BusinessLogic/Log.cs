﻿using System;
using System.Collections.Generic;
using BE = Kiosko.Library.BusinessEntity;
using DA = Kiosko.Library.DataAccess;

namespace Kiosko.Library.BusinessLogic
{
    public class Log
    {

        /// <summary>
        /// Listar el ultimo monitoreo del kioskos
        /// </summary>
        /// <returns></returns>
        public List<BE.Log> Listar(string hostname, DateTime fecha)
        {
            try
            {
                var lstBeLogs = new DA.Log().Listar(hostname, fecha);

                return lstBeLogs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
    }
}
