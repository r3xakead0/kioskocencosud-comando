﻿using System.Collections.Generic;
using System.Data;
using System;
using BE = Kiosko.Library.BusinessEntity;

namespace Kiosko.Library.DataAccess
{
    public class Kiosko
    {

        public List<BE.Kiosko> ListarMonitoreo()
        {
            try
            {
                var lstBeKioskos = new List<BE.Kiosko>();

                string query = @"SELECT	T1.Empresa, 
		                                T1.Tienda, 
		                                T1.Hostname, 
		                                T1.Ip, 
		                                CASE WHEN ISNULL(T1.Ip,'') = '' THEN 'NO' ELSE 'SI' END AS Conectado, 
		                                CASE WHEN ISNULL(T1.[Version],'') = '' THEN 'NO' ELSE 'SI' END AS Encendido, 
		                                T1.[Version], 
		                                T1.Sesion, 
		                                T1.FechaHora  
                                FROM	Auditoria.Kiosko_Monitoreo T1 WITH(NOLOCK)
                                INNER JOIN (
                                SELECT	T0.Hostname, 
		                                MAX(T0.Id) AS Id,
		                                MAX(T0.FechaHora) AS FechaHora
                                FROM	Auditoria.Kiosko_Monitoreo T0 WITH(NOLOCK)
                                WHERE	T0.Hostname IN (SELECT T0.Hostname FROM Perfil.Config T0 WITH(NOLOCK)) 
                                AND     CAST(T0.FechaHora AS DATE) = CAST(GETDATE() AS DATE)
                                GROUP BY T0.Empresa, 
		                                T0.Tienda, 
		                                T0.Hostname
                                ) T2 ON T2.Id = T1.Id 
                                ORDER BY T1.Empresa,
		                                T1.Tienda, 
		                                T1.Hostname";

                var dtKioskos = MssqlHelper.ExecuteQuery(query);

                foreach (DataRow drKiosko in dtKioskos.Rows)
                {
                    var beKiosko = new BE.Kiosko();

                    beKiosko.Empresa = drKiosko["Empresa"].ToString();
                    beKiosko.Tienda = drKiosko["Tienda"].ToString();
                    beKiosko.Hostname = drKiosko["Hostname"].ToString();
                    beKiosko.Ip = drKiosko["Ip"].ToString();
                    beKiosko.Conexion = drKiosko["Conectado"].ToString();
                    beKiosko.Encendido = drKiosko["Encendido"].ToString();
                    beKiosko.Version = drKiosko["Version"].ToString();
                    beKiosko.Sesion = drKiosko["Sesion"].ToString();
                    beKiosko.FechaHora = DateTime.Parse(drKiosko["FechaHora"].ToString());

                    lstBeKioskos.Add(beKiosko);
                }

                return lstBeKioskos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public BE.Kiosko ObtenerMonitoreo(string hostname)
        {
            try
            {
                BE.Kiosko beKiosko = null;

                string template = @"SELECT	TOP 1 
                                            T1.Empresa, 
		                                    T1.Tienda, 
		                                    T1.Hostname, 
		                                    T1.Ip, 
		                                    CASE WHEN ISNULL(T1.Ip,'') = '' THEN 'NO' ELSE 'SI' END AS Conectado, 
		                                    CASE WHEN ISNULL(T1.[Version],'') = '' THEN 'NO' ELSE 'SI' END AS Encendido, 
		                                    T1.[Version], 
		                                    T1.Sesion, 
		                                    T1.FechaHora  
                                    FROM	Auditoria.Kiosko_Monitoreo T1 WITH(NOLOCK)
                                    INNER JOIN (
                                    SELECT	T0.Hostname, 
		                                    MAX(T0.Id) AS Id,
		                                    MAX(T0.FechaHora) AS FechaHora
                                    FROM	Auditoria.Kiosko_Monitoreo T0 WITH(NOLOCK)
                                    WHERE	T0.Hostname = '{0}'
                                    GROUP BY T0.Empresa, 
		                                    T0.Tienda, 
		                                    T0.Hostname
                                    ) T2 ON T2.Id = T1.Id";

                string query = String.Format(template, hostname);

                var dtKioskos = MssqlHelper.ExecuteQuery(query);

                if (dtKioskos.Rows.Count == 1)
                {
                    var drKiosko = dtKioskos.Rows[0];

                    beKiosko = new BE.Kiosko();

                    beKiosko.Empresa = drKiosko["Empresa"].ToString();
                    beKiosko.Tienda = drKiosko["Tienda"].ToString();
                    beKiosko.Hostname = drKiosko["Hostname"].ToString();
                    beKiosko.Ip = drKiosko["Ip"].ToString();
                    beKiosko.Conexion = drKiosko["Conectado"].ToString();
                    beKiosko.Encendido = drKiosko["Encendido"].ToString();
                    beKiosko.Version = drKiosko["Version"].ToString();
                    beKiosko.Sesion = drKiosko["Sesion"].ToString();
                    beKiosko.FechaHora = DateTime.Parse(drKiosko["FechaHora"].ToString());
                }
                return beKiosko;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
