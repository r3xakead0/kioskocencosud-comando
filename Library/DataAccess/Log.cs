﻿using System.Collections.Generic;
using System.Data;
using System;
using BE = Kiosko.Library.BusinessEntity;

namespace Kiosko.Library.DataAccess
{
    public class Log
    {

        public List<BE.Log> Listar(string hostname, DateTime fecha)
        {
            try
            {
                var lstBeLogs = new List<BE.Log>();

                string template = @"SELECT	T1.Compania,
		                                    T0.Tienda,
		                                    T0.Hostname,
		                                    CASE WHEN T0.Tipo = 1 THEN 'TECNICO' ELSE 'USUARIO' END AS Tipo,
		                                    T0.Fecha AS FechaHora,
		                                    T0.Mensaje,
		                                    T0.Descripcion 
                                    FROM	Auditoria.Kiosko_Log T0 WITH(NOLOCK)
                                    INNER JOIN Perfil.Config T1 WITH(NOLOCK) ON T1.Tienda = T0.Tienda
                                    WHERE	T0.Hostname = '{0}' 
                                    AND		CAST(T0.FECHA AS DATE) = '{1}' 
                                    ORDER BY T0.Fecha DESC";

                string query = String.Format(template, hostname, fecha.ToString("yyyy-MM-dd"));

                var dtLogs = MssqlHelper.ExecuteQuery(query);

                foreach (DataRow drLog in dtLogs.Rows)
                {
                    var beLog = new BE.Log();

                    beLog.Empresa = drLog["Compania"].ToString();
                    beLog.Tienda = drLog["Tienda"].ToString();
                    beLog.Hostname = drLog["Hostname"].ToString();
                    beLog.Tipo = drLog["Tipo"].ToString();
                    beLog.Mensaje = drLog["Mensaje"].ToString();
                    beLog.Descripcion = drLog["Descripcion"].ToString();
                    beLog.FechaHora = DateTime.Parse(drLog["FechaHora"].ToString());

                    lstBeLogs.Add(beLog);
                }

                return lstBeLogs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
    }
}
