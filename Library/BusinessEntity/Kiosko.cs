﻿using Newtonsoft.Json;
using System;

namespace Kiosko.Library.BusinessEntity
{
    public class Kiosko
    {
        public string Empresa { get; set; } = "";
        public string Tienda { get; set; } = "";
        public string Hostname { get; set; } = "";
        public string Ip { get; set; } = "";
        public string Conexion { get; set; } = "NO";
        public string Encendido { get; set; } = "NO";
        public string Version { get; set; } = "";
        public string Sesion { get; set; } = "";
        public DateTime FechaHora { get; set; }

        public override string ToString()
        {
            return Hostname;
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}
