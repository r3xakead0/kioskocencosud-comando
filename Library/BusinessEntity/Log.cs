﻿using Newtonsoft.Json;
using System;

namespace Kiosko.Library.BusinessEntity
{
    public class Log
    {
        public string Empresa { get; set; } = "";
        public string Tienda { get; set; } = "";
        public string Hostname { get; set; } = "";
        public string Tipo { get; set; } = "";
        public string Mensaje { get; set; } = "";
        public string Descripcion { get; set; } = "";
        public DateTime FechaHora { get; set; }

        public override string ToString()
        {
            return $"{Hostname} : {Mensaje}";
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}
