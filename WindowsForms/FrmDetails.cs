﻿using MaterialSkin.Controls;
using MaterialSkin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Drawing;
using BE = Kiosko.Library.BusinessEntity;
using BL = Kiosko.Library.BusinessLogic;
using System.Reflection;

namespace Kiosko.WindowsForms
{
    public partial class FrmDetails : MaterialForm
    {

        private readonly MaterialSkinManager materialSkinManager;

        #region "Singletons"

        private static FrmDetails frmInstance = null;

        public static FrmDetails Instance()
        {

            if (frmInstance == null || frmInstance.IsDisposed == true)
            {
                frmInstance = new FrmDetails();
            }

            frmInstance.BringToFront();

            return frmInstance;
        }

        #endregion

        public FrmDetails()
        {
            InitializeComponent();

            #region Initialize MaterialSkinManager
            materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            #endregion
        }

        private void FrmDetails_Load(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        public void Details(string hostname)
        {
            try
            {

                BE.Kiosko beKiosko = new BL.Kiosko().ObtenerMonitoreo(hostname);

                var dtKiosko = ObjectToData(beKiosko);
                this.dgvDetails.DataSource = dtKiosko;

                Util.FormatDatagridview(ref dgvDetails);
                Util.AutoWidthColumn(ref dgvDetails, "Valor");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private DataTable ObjectToData(object obj)
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Propiedad");
                dt.Columns.Add("Valor");

                obj.GetType().GetProperties().ToList().ForEach(f =>
                {
                    try
                    {
                        DataRow dr = dt.NewRow();
                        dr["Propiedad"] = f.Name;
                        dr["Valor"] = f.GetValue(obj, null);
                        dt.Rows.Add(dr);
                    }
                    catch (Exception exInner)
                    {
                        throw exInner;
                    }
                });
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

}
