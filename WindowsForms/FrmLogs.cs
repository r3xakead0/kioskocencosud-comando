﻿using MaterialSkin.Controls;
using MaterialSkin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Drawing;
using BE = Kiosko.Library.BusinessEntity;
using BL = Kiosko.Library.BusinessLogic;
using System.Reflection;

namespace Kiosko.WindowsForms
{
    public partial class FrmLogs : MaterialForm
    {

        private readonly MaterialSkinManager materialSkinManager;

        private string hostname = "";

        #region "Singletons"

        private static FrmLogs frmInstance = null;

        public static FrmLogs Instance()
        {

            if (frmInstance == null || frmInstance.IsDisposed == true)
            {
                frmInstance = new FrmLogs();
            }

            frmInstance.BringToFront();

            return frmInstance;
        }

        #endregion

        public FrmLogs()
        {
            InitializeComponent();

            #region Initialize MaterialSkinManager
            materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            #endregion
        }

        private void FrmLogs_Load(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        public void Details(string hostname)
        {
            try
            {

                this.hostname = hostname;
                this.dtpFecha.Value = DateTime.Now;

                var lstBeLogs = new BL.Log().Listar(this.hostname, this.dtpFecha.Value);
                this.dgvDetails.DataSource = new SortableBindingList<BE.Log>(lstBeLogs);

                Util.FormatDatagridview(ref dgvDetails);

                this.dgvDetails.Columns["Empresa"].Visible = false;
                this.dgvDetails.Columns["Tienda"].Visible = false;
                this.dgvDetails.Columns["Hostname"].Visible = false;

                this.dgvDetails.Columns["Tipo"].Visible = true;
                this.dgvDetails.Columns["Tipo"].HeaderText = "Tipo";
                this.dgvDetails.Columns["Tipo"].Width = 100;
                this.dgvDetails.Columns["Tipo"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                     
                this.dgvDetails.Columns["Mensaje"].Visible = true;
                this.dgvDetails.Columns["Mensaje"].HeaderText = "Mensaje";
                this.dgvDetails.Columns["Mensaje"].Width = 100;
                this.dgvDetails.Columns["Mensaje"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                     
                this.dgvDetails.Columns["Descripcion"].Visible = true;
                this.dgvDetails.Columns["Descripcion"].HeaderText = "Descripcion";
                this.dgvDetails.Columns["Descripcion"].Width = 150;
                this.dgvDetails.Columns["Descripcion"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                     
                this.dgvDetails.Columns["FechaHora"].Visible = true;
                this.dgvDetails.Columns["FechaHora"].HeaderText = "Fecha Hora";
                this.dgvDetails.Columns["FechaHora"].DefaultCellStyle.Format = "dd/MM/yyyy HH:mm:ss";
                this.dgvDetails.Columns["FechaHora"].Width = 150;
                this.dgvDetails.Columns["FechaHora"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                Util.AutoWidthColumn(ref dgvDetails, "Descripcion");

                this.txtEmpresa.Clear();
                this.txtTienda.Clear();
                this.txtHostname.Clear();
                this.txtTipo.Clear();
                this.txtMensaje.Clear();
                this.txtDescripcion.Clear();
                this.txtFechaHora.Clear();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        private void btnSalir_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void dgvDetails_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex < 0)
                    return;

                var beLog = (BE.Log)this.dgvDetails.CurrentRow.DataBoundItem;

                this.txtEmpresa.Text = beLog.Empresa;
                this.txtTienda.Text = beLog.Tienda;
                this.txtHostname.Text = beLog.Hostname;
                this.txtTipo.Text = beLog.Tipo;
                this.txtMensaje.Text = beLog.Mensaje;
                this.txtDescripcion.Text = beLog.Descripcion;
                this.txtFechaHora.Text = beLog.FechaHora.ToString("dd/MM/yyyy HH:mm:ss");
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void dtpFecha_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                var lstBeLogs = new BL.Log().Listar(this.hostname, this.dtpFecha.Value);
                this.dgvDetails.DataSource = new SortableBindingList<BE.Log>(lstBeLogs);

                this.txtEmpresa.Clear();
                this.txtTienda.Clear();
                this.txtHostname.Clear();
                this.txtTipo.Clear();
                this.txtMensaje.Clear();
                this.txtDescripcion.Clear();
                this.txtFechaHora.Clear();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }

}
