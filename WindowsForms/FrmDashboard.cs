﻿using MaterialSkin.Controls;
using MaterialSkin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Drawing;
using BE = Kiosko.Library.BusinessEntity;
using BL = Kiosko.Library.BusinessLogic;

namespace Kiosko.WindowsForms
{
    public partial class FrmDashboard : MaterialForm
    {

        private List<BE.Kiosko> lstKioskosCompletos = new List<BE.Kiosko>();

        private readonly MaterialSkinManager materialSkinManager;
        private int colorSchemeIndex;

        private Timer tmrActualizar = null;

        public FrmDashboard()
        {
            InitializeComponent();

            #region Initialize MaterialSkinManager
            materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            #endregion

            #region Timmer Auto Reload
            this.tmrActualizar = new Timer();
            this.tmrActualizar.Interval = 60 * 1000;
            this.tmrActualizar.Enabled = false;
            this.tmrActualizar.Tick += new EventHandler(this.tmrActualizar_tick);
            #endregion
        }

        private void tmrActualizar_tick(object sender, EventArgs e)
        {
            try
            {
                this.ListarMonitoreo();
                this.GraficoMonitoreo();
                this.ResumenMonitoreo();
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void FrmDashboard_Load(object sender, EventArgs e)
        {
            try
            {

                this.lblVersion.Text = $"VERSIÓN { Application.ProductVersion }";

                this.CargarEmpresas();

                #region Template List 
                var lstBeKioskos = new List<BE.Kiosko>();
                var sorted = new SortableBindingList<BE.Kiosko>(lstBeKioskos);
                this.dgvKioskos.DataSource = sorted;

                this.FormatoMonitoreo();
                #endregion

                this.ListarMonitoreo();
                this.GraficoMonitoreo();
                this.ResumenMonitoreo();

                this.tmrActualizar.Start();
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        /// <summary>
        /// Cambiar el color del formulario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnColor_Click(object sender, EventArgs e)
        {
            try
            {
                colorSchemeIndex++;
                if (colorSchemeIndex > 2) colorSchemeIndex = 0;

                //These are just example color schemes
                switch (colorSchemeIndex)
                {
                    case 0:
                        materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
                        break;
                    case 1:
                        materialSkinManager.ColorScheme = new ColorScheme(Primary.Indigo500, Primary.Indigo700, Primary.Indigo100, Accent.Pink200, TextShade.WHITE);
                        break;
                    case 2:
                        materialSkinManager.ColorScheme = new ColorScheme(Primary.Green600, Primary.Green700, Primary.Green200, Accent.Red100, TextShade.WHITE);
                        break;
                }
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        /// <summary>
        /// Cambiar el diseño del formulario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTema_Click(object sender, EventArgs e)
        {
            try
            {
                materialSkinManager.Theme = materialSkinManager.Theme == MaterialSkinManager.Themes.DARK ? MaterialSkinManager.Themes.LIGHT : MaterialSkinManager.Themes.DARK;
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        /// <summary>
        /// Seleccionar la empresa para filtrar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cboEmpresa_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                this.btnRefrescar_Click(null, null);
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        /// <summary>
        /// Grafico de Monitoreo de kioskos Conectados y Desconectados
        /// </summary>
        private void GraficoMonitoreo()
        {
            try
            {
                if (this.lstKioskosCompletos.Count > 0)
                {

                    Font font = new Font(this.Font.FontFamily, 12.0f, FontStyle.Bold);

                    string[] x;
                    int[] y;

                    #region Grafico de Conectados y Desconectados

                    var lstBeKioskosConexion = this.lstKioskosCompletos
                                                .GroupBy(a => a.Conexion)
                                                .Select(a => new
                                                {
                                                    Conexion = a.Key,
                                                    Total = a.Count()
                                                });

                    x = (from p in lstBeKioskosConexion
                                  orderby p.Conexion ascending
                                  select p.Conexion).ToArray();

                    y = (from p in lstBeKioskosConexion
                               orderby p.Conexion ascending
                               select p.Total).ToArray();


                    this.chrKioskosConectados.Titles.Clear();
                    this.chrKioskosConectados.Titles.Add("CONECTADOS");
                    this.chrKioskosConectados.Titles[0].Font = font;

                    this.chrKioskosConectados.Series[0].ChartType = SeriesChartType.Doughnut;
                    this.chrKioskosConectados.Series[0].Points.DataBindXY(x, y);
                    this.chrKioskosConectados.Series[0].IsValueShownAsLabel = true;
                    this.chrKioskosConectados.Series[0].Label = "#PERCENT{P2}";

                    this.chrKioskosConectados.Series[0].Points[0].Color = Color.Red;
                    this.chrKioskosConectados.Series[0].Points[1].Color = Color.Green;

                    this.chrKioskosConectados.ChartAreas[0].Area3DStyle.Enable3D = true;

                    #endregion

                    #region Grafico de Encendidos y Apagados

                    var lstBeKioskosEncendido = this.lstKioskosCompletos
                                                .GroupBy(a => a.Encendido)
                                                .Select(a => new
                                                {
                                                    Encendido = a.Key,
                                                    Total = a.Count()
                                                });

                    x = (from p in lstBeKioskosEncendido
                         orderby p.Encendido ascending
                         select p.Encendido).ToArray();

                    y = (from p in lstBeKioskosEncendido
                         orderby p.Encendido ascending
                         select p.Total).ToArray();


                    this.chrKioskosEncendidos.Titles.Clear();
                    this.chrKioskosEncendidos.Titles.Add("ENCENDIDOS");
                    this.chrKioskosEncendidos.Titles[0].Font = font;

                    this.chrKioskosEncendidos.Series[0].ChartType = SeriesChartType.Pie;
                    this.chrKioskosEncendidos.Series[0].Points.DataBindXY(x, y);
                    this.chrKioskosEncendidos.Series[0].IsValueShownAsLabel = true;
                    this.chrKioskosEncendidos.Series[0].Label = "#PERCENT{P2}";

                    this.chrKioskosEncendidos.Series[0].Points[0].Color = Color.Red;
                    this.chrKioskosEncendidos.Series[0].Points[1].Color = Color.Green;

                    this.chrKioskosEncendidos.ChartAreas[0].Area3DStyle.Enable3D = true;

                    #endregion
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Listado de ultimo Monitoreo de kioskos
        /// </summary>
        private void CargarEmpresas()
        {
            try
            {
                this.cboEmpresa.Items.Clear();

                this.cboEmpresa.Items.Add("TODOS");
                this.cboEmpresa.Items.Add("WONG");
                this.cboEmpresa.Items.Add("METRO");

                this.cboEmpresa.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Listado de ultimo Monitoreo de kioskos
        /// </summary>
        private void ListarMonitoreo()
        {
            try
            {
                this.lstKioskosCompletos = new BL.Kiosko().ListarMonitoreo();

                if (this.cboEmpresa.SelectedIndex > 0)
                {
                    string empresa = this.cboEmpresa.SelectedItem.ToString().ToUpper();
                    this.lstKioskosCompletos = this.lstKioskosCompletos.Where(x => x.Empresa.ToUpper().Equals(empresa)).ToList();
                }

                this.dgvKioskos.DataSource = new SortableBindingList<BE.Kiosko>(this.lstKioskosCompletos);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Resumen de Monitoreo de kioskos Conectados y Desconectados
        /// </summary>
        private void ResumenMonitoreo()
        {
            try
            {

                int cntEncendidos = this.lstKioskosCompletos.Where(x => x.Encendido.ToUpper().Equals("SI")).Count();
                int cntApagados = this.lstKioskosCompletos.Where(x => x.Encendido.ToUpper().Equals("NO")).Count();

                int cntConectados = this.lstKioskosCompletos.Where(x => x.Conexion.ToUpper().Equals("SI")).Count();
                int cntDesconectados = this.lstKioskosCompletos.Where(x => x.Conexion.ToUpper().Equals("NO")).Count();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void FormatoMonitoreo()
        {
            try
            {
                Util.FormatDatagridview(ref dgvKioskos);

                this.dgvKioskos.Columns["Empresa"].Visible = true;
                this.dgvKioskos.Columns["Empresa"].HeaderText = "Empresa";
                this.dgvKioskos.Columns["Empresa"].Width = 70;
                this.dgvKioskos.Columns["Empresa"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

                this.dgvKioskos.Columns["Tienda"].Visible = true;
                this.dgvKioskos.Columns["Tienda"].HeaderText = "Tienda";
                this.dgvKioskos.Columns["Tienda"].Width = 70;
                this.dgvKioskos.Columns["Tienda"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

                this.dgvKioskos.Columns["Hostname"].Visible = true;
                this.dgvKioskos.Columns["Hostname"].HeaderText = "Hostname";
                this.dgvKioskos.Columns["Hostname"].Width = 80;
                this.dgvKioskos.Columns["Hostname"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

                this.dgvKioskos.Columns["Ip"].Visible = true;
                this.dgvKioskos.Columns["Ip"].HeaderText = "IP";
                this.dgvKioskos.Columns["Ip"].Width = 100;
                this.dgvKioskos.Columns["Ip"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                this.dgvKioskos.Columns["Conexion"].Visible = true;
                this.dgvKioskos.Columns["Conexion"].HeaderText = "Conexion";
                this.dgvKioskos.Columns["Conexion"].Width = 70;
                this.dgvKioskos.Columns["Conexion"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                this.dgvKioskos.Columns["Encendido"].Visible = true;
                this.dgvKioskos.Columns["Encendido"].HeaderText = "Encendido";
                this.dgvKioskos.Columns["Encendido"].Width = 70;
                this.dgvKioskos.Columns["Encendido"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                this.dgvKioskos.Columns["Version"].Visible = true;
                this.dgvKioskos.Columns["Version"].HeaderText = "Version";
                this.dgvKioskos.Columns["Version"].Width = 70;
                this.dgvKioskos.Columns["Version"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                this.dgvKioskos.Columns["Sesion"].Visible = false;
                this.dgvKioskos.Columns["FechaHora"].Visible = false;

                DataGridViewButtonColumn btnRestart = new DataGridViewButtonColumn();
                {
                    btnRestart.Name = "btnRestart";
                    btnRestart.HeaderText = "";
                    btnRestart.Text = "Reiniciar";
                    btnRestart.Width = 70;
                    btnRestart.UseColumnTextForButtonValue = true; 
                    this.dgvKioskos.Columns.Add(btnRestart);
                }

                DataGridViewButtonColumn btnStatus = new DataGridViewButtonColumn();
                {
                    btnStatus.Name = "btnStatus";
                    btnStatus.HeaderText = "";
                    btnStatus.Text = "Detalle";
                    btnStatus.Width = 70;
                    btnStatus.UseColumnTextForButtonValue = true;
                    this.dgvKioskos.Columns.Add(btnStatus);
                }

                DataGridViewButtonColumn btnLogs = new DataGridViewButtonColumn();
                {
                    btnLogs.Name = "btnLogs";
                    btnLogs.HeaderText = "";
                    btnLogs.Text = "Errores";
                    btnLogs.Width = 70;
                    btnLogs.UseColumnTextForButtonValue = true;
                    this.dgvKioskos.Columns.Add(btnLogs);
                }

                Util.AutoWidthColumn(ref dgvKioskos, "Hostname");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        private void dgvKioskos_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                foreach (DataGridViewRow Myrow in dgvKioskos.Rows)
                {
                    if (Myrow.Cells["Conexion"].Value.Equals("SI"))
                    {
                        Myrow.Cells["Conexion"].Style.ForeColor = Color.White;
                        Myrow.Cells["Conexion"].Style.BackColor = Color.Green;
                    }
                    else
                    {
                        Myrow.Cells["Conexion"].Style.ForeColor = Color.White;
                        Myrow.Cells["Conexion"].Style.BackColor = Color.Red;
                    }

                    if (Myrow.Cells["Encendido"].Value.Equals("SI"))
                    {
                        Myrow.Cells["Encendido"].Style.ForeColor = Color.White;
                        Myrow.Cells["Encendido"].Style.BackColor = Color.Green;
                    }
                    else
                    {
                        Myrow.Cells["Encendido"].Style.ForeColor = Color.White;
                        Myrow.Cells["Encendido"].Style.BackColor = Color.Red;
                    }
                }
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void dgvKioskos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                var senderGrid = (DataGridView)sender;

                if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                    e.RowIndex >= 0)
                {
                    string conectado = senderGrid.Rows[e.RowIndex].Cells["Conexion"].Value.ToString().ToUpper();
                    string encendido = senderGrid.Rows[e.RowIndex].Cells["Encendido"].Value.ToString().ToUpper();
                    if (conectado.Equals("SI") && encendido.Equals("SI"))
                    {

                        string hostname = senderGrid.Rows[e.RowIndex].Cells["Hostname"].Value.ToString();
                        if (senderGrid.Columns[e.ColumnIndex].Name == "btnRestart")
                        {
                            if (Util.ConfirmationMessage("¿Desea reiniciar el kiosko remotamente?"))
                            {
                                bool rpta = new BL.Kiosko().Reiniciar(hostname);
                                if (rpta)
                                {
                                    senderGrid.Rows[e.RowIndex].Cells["Ip"].Value = "";
                                    senderGrid.Rows[e.RowIndex].Cells["Conexion"].Value = "NO";
                                    senderGrid.Rows[e.RowIndex].Cells["Encendido"].Value = "NO";
                                    senderGrid.Rows[e.RowIndex].Cells["Version"].Value = "";
                                    senderGrid.Rows[e.RowIndex].Cells["Sesion"].Value = "";
                                    senderGrid.Rows[e.RowIndex].Cells["FechaHora"].Value = DateTime.Now;

                                    Util.InformationMessage("El Kiosko se esta reiniciando");
                                }
                                else
                                    Util.InformationMessage("El Kiosko esta ocupado");
                            }
                        }
                        else if (senderGrid.Columns[e.ColumnIndex].Name == "btnStatus")
                        {
                            var details = FrmDetails.Instance();
                            details.Details(hostname);
                            details.ShowDialog();
                        }
                        else if (senderGrid.Columns[e.ColumnIndex].Name == "btnLogs")
                        {
                            var logs = FrmLogs.Instance();
                            logs.Details(hostname);
                            logs.ShowDialog();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void FrmDashboard_ResizeEnd(object sender, EventArgs e)
        {
            try
            {
                Util.AutoWidthColumn(ref dgvKioskos, "Hostname");
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void btnRefrescar_Click(object sender, EventArgs e)
        {
            try
            {
                this.tmrActualizar.Stop();

                this.ListarMonitoreo();
                this.GraficoMonitoreo();
                this.ResumenMonitoreo();

                this.tmrActualizar.Start();
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

    }

}
