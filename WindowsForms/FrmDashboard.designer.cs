﻿namespace Kiosko.WindowsForms
{
    partial class FrmDashboard
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDashboard));
            this.dgvKioskos = new System.Windows.Forms.DataGridView();
            this.lblResumen = new System.Windows.Forms.Label();
            this.chrKioskosConectados = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.materialDivider1 = new MaterialSkin.Controls.MaterialDivider();
            this.btnTema = new MaterialSkin.Controls.MaterialRaisedButton();
            this.btnColor = new MaterialSkin.Controls.MaterialRaisedButton();
            this.lblMonitoreo = new System.Windows.Forms.Label();
            this.chrKioskosEncendidos = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.btnRefrescar = new MaterialSkin.Controls.MaterialRaisedButton();
            this.lblEmpresa = new System.Windows.Forms.Label();
            this.cboEmpresa = new System.Windows.Forms.ComboBox();
            this.lblVersion = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKioskos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chrKioskosConectados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chrKioskosEncendidos)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvKioskos
            // 
            this.dgvKioskos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvKioskos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvKioskos.Location = new System.Drawing.Point(12, 128);
            this.dgvKioskos.Name = "dgvKioskos";
            this.dgvKioskos.Size = new System.Drawing.Size(794, 591);
            this.dgvKioskos.TabIndex = 6;
            this.dgvKioskos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvKioskos_CellContentClick);
            this.dgvKioskos.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvKioskos_CellFormatting);
            // 
            // lblResumen
            // 
            this.lblResumen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblResumen.BackColor = System.Drawing.Color.Navy;
            this.lblResumen.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResumen.ForeColor = System.Drawing.Color.White;
            this.lblResumen.Location = new System.Drawing.Point(812, 77);
            this.lblResumen.Name = "lblResumen";
            this.lblResumen.Size = new System.Drawing.Size(200, 21);
            this.lblResumen.TabIndex = 18;
            this.lblResumen.Text = "Resumen";
            this.lblResumen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chrKioskosConectados
            // 
            this.chrKioskosConectados.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chrKioskosConectados.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Center;
            this.chrKioskosConectados.BorderlineColor = System.Drawing.Color.Black;
            this.chrKioskosConectados.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea1.Name = "ChartArea1";
            this.chrKioskosConectados.ChartAreas.Add(chartArea1);
            legend1.Alignment = System.Drawing.StringAlignment.Center;
            legend1.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            legend1.Name = "Legend1";
            this.chrKioskosConectados.Legends.Add(legend1);
            this.chrKioskosConectados.Location = new System.Drawing.Point(812, 101);
            this.chrKioskosConectados.Name = "chrKioskosConectados";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Doughnut;
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chrKioskosConectados.Series.Add(series1);
            this.chrKioskosConectados.Size = new System.Drawing.Size(200, 275);
            this.chrKioskosConectados.TabIndex = 10;
            // 
            // materialDivider1
            // 
            this.materialDivider1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.materialDivider1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialDivider1.Depth = 0;
            this.materialDivider1.Location = new System.Drawing.Point(0, 725);
            this.materialDivider1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider1.Name = "materialDivider1";
            this.materialDivider1.Size = new System.Drawing.Size(1024, 1);
            this.materialDivider1.TabIndex = 14;
            this.materialDivider1.Text = "materialDivider1";
            // 
            // btnTema
            // 
            this.btnTema.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTema.Depth = 0;
            this.btnTema.Location = new System.Drawing.Point(873, 732);
            this.btnTema.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnTema.Name = "btnTema";
            this.btnTema.Primary = true;
            this.btnTema.Size = new System.Drawing.Size(139, 24);
            this.btnTema.TabIndex = 15;
            this.btnTema.Text = "Tema";
            this.btnTema.UseVisualStyleBackColor = true;
            this.btnTema.Click += new System.EventHandler(this.btnTema_Click);
            // 
            // btnColor
            // 
            this.btnColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnColor.Depth = 0;
            this.btnColor.Location = new System.Drawing.Point(728, 732);
            this.btnColor.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnColor.Name = "btnColor";
            this.btnColor.Primary = true;
            this.btnColor.Size = new System.Drawing.Size(139, 24);
            this.btnColor.TabIndex = 16;
            this.btnColor.Text = "Color";
            this.btnColor.UseVisualStyleBackColor = true;
            this.btnColor.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // lblMonitoreo
            // 
            this.lblMonitoreo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMonitoreo.BackColor = System.Drawing.Color.Navy;
            this.lblMonitoreo.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMonitoreo.ForeColor = System.Drawing.Color.White;
            this.lblMonitoreo.Location = new System.Drawing.Point(12, 77);
            this.lblMonitoreo.Name = "lblMonitoreo";
            this.lblMonitoreo.Size = new System.Drawing.Size(794, 21);
            this.lblMonitoreo.TabIndex = 19;
            this.lblMonitoreo.Text = "Monitoreo de Kioskos";
            this.lblMonitoreo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chrKioskosEncendidos
            // 
            this.chrKioskosEncendidos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chrKioskosEncendidos.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Center;
            this.chrKioskosEncendidos.BorderlineColor = System.Drawing.Color.Black;
            this.chrKioskosEncendidos.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea2.Name = "ChartArea1";
            this.chrKioskosEncendidos.ChartAreas.Add(chartArea2);
            legend2.Alignment = System.Drawing.StringAlignment.Center;
            legend2.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            legend2.Name = "Legend1";
            this.chrKioskosEncendidos.Legends.Add(legend2);
            this.chrKioskosEncendidos.Location = new System.Drawing.Point(812, 382);
            this.chrKioskosEncendidos.Name = "chrKioskosEncendidos";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Doughnut;
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.chrKioskosEncendidos.Series.Add(series2);
            this.chrKioskosEncendidos.Size = new System.Drawing.Size(200, 275);
            this.chrKioskosEncendidos.TabIndex = 20;
            // 
            // btnRefrescar
            // 
            this.btnRefrescar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefrescar.Depth = 0;
            this.btnRefrescar.Location = new System.Drawing.Point(812, 695);
            this.btnRefrescar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnRefrescar.Name = "btnRefrescar";
            this.btnRefrescar.Primary = true;
            this.btnRefrescar.Size = new System.Drawing.Size(200, 24);
            this.btnRefrescar.TabIndex = 21;
            this.btnRefrescar.Text = "Refrescar";
            this.btnRefrescar.UseVisualStyleBackColor = true;
            this.btnRefrescar.Click += new System.EventHandler(this.btnRefrescar_Click);
            // 
            // lblEmpresa
            // 
            this.lblEmpresa.AutoSize = true;
            this.lblEmpresa.Location = new System.Drawing.Point(12, 104);
            this.lblEmpresa.Name = "lblEmpresa";
            this.lblEmpresa.Size = new System.Drawing.Size(57, 13);
            this.lblEmpresa.TabIndex = 22;
            this.lblEmpresa.Text = "Empresa";
            // 
            // cboEmpresa
            // 
            this.cboEmpresa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboEmpresa.FormattingEnabled = true;
            this.cboEmpresa.Location = new System.Drawing.Point(75, 101);
            this.cboEmpresa.Name = "cboEmpresa";
            this.cboEmpresa.Size = new System.Drawing.Size(121, 21);
            this.cboEmpresa.TabIndex = 23;
            this.cboEmpresa.SelectionChangeCommitted += new System.EventHandler(this.cboEmpresa_SelectionChangeCommitted);
            // 
            // lblVersion
            // 
            this.lblVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblVersion.AutoSize = true;
            this.lblVersion.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersion.Location = new System.Drawing.Point(12, 736);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(127, 16);
            this.lblVersion.TabIndex = 24;
            this.lblVersion.Text = "VERSIÓN 1.0.0.0";
            // 
            // FrmDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.cboEmpresa);
            this.Controls.Add(this.lblEmpresa);
            this.Controls.Add(this.btnRefrescar);
            this.Controls.Add(this.chrKioskosEncendidos);
            this.Controls.Add(this.lblMonitoreo);
            this.Controls.Add(this.btnColor);
            this.Controls.Add(this.btnTema);
            this.Controls.Add(this.materialDivider1);
            this.Controls.Add(this.lblResumen);
            this.Controls.Add(this.chrKioskosConectados);
            this.Controls.Add(this.dgvKioskos);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmDashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DASHBOARD";
            this.Load += new System.EventHandler(this.FrmDashboard_Load);
            this.ResizeEnd += new System.EventHandler(this.FrmDashboard_ResizeEnd);
            ((System.ComponentModel.ISupportInitialize)(this.dgvKioskos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chrKioskosConectados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chrKioskosEncendidos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dgvKioskos;
        private MaterialSkin.Controls.MaterialDivider materialDivider1;
        private MaterialSkin.Controls.MaterialRaisedButton btnTema;
        private MaterialSkin.Controls.MaterialRaisedButton btnColor;
        private System.Windows.Forms.Label lblResumen;
        private System.Windows.Forms.DataVisualization.Charting.Chart chrKioskosConectados;
        private System.Windows.Forms.Label lblMonitoreo;
        private System.Windows.Forms.DataVisualization.Charting.Chart chrKioskosEncendidos;
        private MaterialSkin.Controls.MaterialRaisedButton btnRefrescar;
        private System.Windows.Forms.Label lblEmpresa;
        private System.Windows.Forms.ComboBox cboEmpresa;
        private System.Windows.Forms.Label lblVersion;
    }
}

