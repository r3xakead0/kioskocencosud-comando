﻿using System;
using BE = Kiosko.Library.BusinessEntity;
using BL = Kiosko.Library.BusinessLogic;
using NA = Kiosko.Library.NetworkAccess;
using ConTabs;

namespace Kiosko.Terminal
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {

                string hostname = "";
                string command = "";

                if (args.Length == 2)
                {
                    hostname = args[0].ToUpper();
                    command = args[1].ToUpper();

                    string response = "";

                    switch (command)
                    {
                        case "RESTART":
                            response = new NA.Kiosko().Restart(hostname);
                            break;
                        case "STATUS":
                            var beKiosko = new BL.Kiosko().Estado(hostname);
                            if (beKiosko != null)
                                response = beKiosko.ToJson();
                            break;
                        case "LOG":
                            var lstLogs = new BL.Log().Listar(hostname, DateTime.Now);

                            var logs = Table<BE.Log>.Create(lstLogs);

                            logs.Columns["Empresa"].Hide = true;
                            logs.Columns["Tienda"].Hide = true;
                            logs.Columns["Hostname"].Hide = true;
                            logs.Columns["Tipo"].Alignment = Alignment.Center;
                            logs.Columns["Mensaje"].LongStringBehaviour = LongStringBehaviour.Wrap;
                            logs.Columns["Mensaje"].Alignment = Alignment.Left;
                            logs.Columns["Descripcion"].LongStringBehaviour = LongStringBehaviour.Wrap;
                            logs.Columns["Descripcion"].Alignment = Alignment.Left;
                            logs.Columns["FechaHora"].FormatString = "dd/MM/yyyy HH:mm:ss";
                            logs.Columns["FechaHora"].Alignment = Alignment.Center;
                            logs.TableStyle = Style.UnicodeLines;

                            Console.WriteLine(logs.ToString());

                            break;
                        default:
                            break;
                    }

                    Console.WriteLine(response);
                }
                else if (args.Length == 1)
                {
                    hostname = args[0].ToUpper();
                    string response = "";

                    var beKiosko = new BL.Kiosko().ObtenerMonitoreo(hostname);
                    if (beKiosko != null)
                    {
                        response = beKiosko.ToJson();
                    }
                    Console.WriteLine(response);
                }
                else
                {
                    var lstKioskos = new BL.Kiosko().ListarMonitoreo();;

                    var kioskos = Table<BE.Kiosko>.Create(lstKioskos);

                    kioskos.Columns["Empresa"].Alignment = Alignment.Left;
                    kioskos.Columns["Tienda"].Alignment = Alignment.Left;
                    kioskos.Columns["Hostname"].Alignment = Alignment.Left;
                    kioskos.Columns["Ip"].Alignment = Alignment.Center;
                    kioskos.Columns["Conexion"].Alignment = Alignment.Center;
                    kioskos.Columns["Encendido"].Alignment = Alignment.Center;
                    kioskos.Columns["Version"].Alignment = Alignment.Center;
                    kioskos.Columns["Sesion"].Alignment = Alignment.Center;
                    kioskos.Columns["FechaHora"].Hide = true;
                    kioskos.TableStyle = Style.UnicodeLines;

                    Console.WriteLine(kioskos.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.ReadKey();
            }
        }
        
    }
}
