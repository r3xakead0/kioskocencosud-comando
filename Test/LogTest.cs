﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BL = Kiosko.Library.BusinessLogic;

namespace Kiosko.Test
{
    [TestClass]
    public class LogTest
    {
        [TestMethod]
        public void ListarTest01()
        {
            string hostname = "W104901KS001"; //Existe
            DateTime fechaHora = DateTime.Parse("01/10/2018");
            var lst = new BL.Log().Listar(hostname, fechaHora);

            Assert.AreNotEqual(lst.Count, 0);
        }

        [TestMethod]
        public void ListarTest02()
        {
            string hostname = "W104901KS111"; //No Existe
            DateTime fechaHora = DateTime.Parse("01/10/2018");
            var lst = new BL.Log().Listar(hostname, fechaHora);

            Assert.AreEqual(lst.Count, 0);
        }
    }
}
